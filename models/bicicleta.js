var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function() {
    return 'id: ' + this.id + '| color: ' + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if(aBici)
        return aBici
    else
        throw new Error(`No existe bicicleta con id: ${aBiciId}`);
}


Bicicleta.removeById = function(aBiciId){
   Bicicleta.findById(aBiciId);
   for(i = 0; i <= Bicicleta.allBicis.length; i++){
    if(aBiciId == Bicicleta.allBicis[i].id){
        Bicicleta.allBicis.splice(i,1);
        break;
    }        
   }
    

}

// var a = new Bicicleta (1, "rojo", "urbana", [-26.4124612,-54.688639]);
// var b = new Bicicleta (2, "blanco", "urbana", [-26.4124612,-54.80]);

// Bicicleta.add(a);
// Bicicleta.add(b);


module.exports = Bicicleta;