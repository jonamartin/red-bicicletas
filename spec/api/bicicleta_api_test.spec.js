var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

describe('Bicicleta API', ()=> {
    describe('GET Bicicletas /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta (1, "rojo", "urbana", [-26.4124612,-54.688639]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', (error, response, body) => {
                expect(response.statusCode).toBe(200);
            })
        })
    });

    describe('POST Bicicletas /create', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type' : 'application/json'};
            var aBici = '{"id": 10, "color":"verde", "modelo":"mountain","latitud":-54,"longitud":-26}';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe('verde');
                done();
            });
        });
    });

    describe('UPDATE Bicicletas/update', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type' : 'application/json'};
            var aBici = '{"id": 10, "color":"rojo", "modelo":"urban","latitud":-54,"longitud":-26}';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/update',
                body: aBici
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe('rojo');
                done();
            });

        }) 
    })

    describe('DELETE Bicicletas/detele', () => {
        it('Status 204', (done) => {
            var headers = { 'content-type' : 'application/json'};
            var aBiciId = '{"id": 10 }'
            request.delete({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: aBiciId
            }, (error, response, body)=> {
                expect(response.statusCode).toBe(204);
                done();
            });
        })
    });


    
});