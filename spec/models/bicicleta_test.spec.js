var Bicicleta = require('../../models/bicicleta');

beforeEach(() => {
    Bicicleta.allBicis = []
});
describe('Bicicleta.allBicis', () => {
    it('Comienza vacio', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    })
})

describe('Bicicleta.add', ()=> {
    it('Agregar una', ()=> {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta (1, "rojo", "urbana", [-26.4124612,-54.688639]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    })
})


describe('Bicicleta.findBy', ()=> {
    it('Debe devolver la bici con id 1', ()=> {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici1 = new Bicicleta(1, 'verde', 'urbana');
        var aBici2 = new Bicicleta(2, 'azul', 'mountain');
        Bicicleta.add(aBici1);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici1.color);
        expect(targetBici.modelo).toBe(aBici1.modelo);
    })
})

describe('Bicicleta.removeById', () => {
    it('Debe devolver la bici con id 2', () => {
        var aBici1 = new Bicicleta(1, 'verde', 'urbana');
        var aBici2 = new Bicicleta(2, 'azul', 'mountain');
        Bicicleta.add(aBici1);
        Bicicleta.add(aBici2);
        Bicicleta.removeById(1);
        
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(aBici2);
    })
})