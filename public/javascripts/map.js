var map = L.map('main_map').setView([-26.4124612,-54.688639],13)

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: (result) => {
        console.log(result)
        result.bicicletas.forEach((bici) => {
            L.marker([bici.ubicacion[0], bici.ubicacion[1]], {title: bici.id}).addTo(map);
        });
    }
});
    